# course-openshift

1. จาก Soucecode
    ```
    oc new-app https://github.com/sclorg/nodejs-ex -l name=myapp
    ```
2. ~~จาก DockerHub Images~~
    ```
    oc new-app nginx
    ```
3. ~~จาก Openshift Templates~~
    ```
    git clone https://github.com/sclorg/nodejs-ex
    oc new-app -f ./openshift/templates/nodejs.json
    ```
4. จาก Openshift Web Console / Web UI

------

- More
    - dev_guide for new app in openshift
        - https://docs.openshift.com/container-platform/3.7/dev_guide/application_lifecycle/new_app.html
    - create new app with passing env
        ```
        oc new-app openshift/postgresql-92-centos7 \
        -e POSTGRESQL_USER=user \
        -e POSTGRESQL_DATABASE=db \
        -e POSTGRESQL_PASSWORD=password
        ```
    - https://github.com/openshift-katacoda/blog-django-py
    
