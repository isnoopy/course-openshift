# course-openshift : first-application

1. สร้าง project ชื่อตัวเอง
```sh
oc login -u system:admin
oc new-project YOUR_NAME --display-name="YOUR_NAME" --description="YOUR_NAME"
```
2. สร้าง application จาก category -> nodejs
    - https://github.com/sclorg/nodejs-ex
3. ดูรายละเอียดต่าง ๆ ที่เกิดขึ้นจากการสร้าง application


