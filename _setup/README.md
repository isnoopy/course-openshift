# course-openshift

- install minishift on local 
    - setting-up-virtualization-environment
        - https://docs.okd.io/3.11/minishift/getting-started/setting-up-virtualization-environment.html
    - install minishift
        - https://docs.okd.io/3.11/minishift/getting-started/installing.html
    - more reference site about minishift
        - https://medium.com/@nutta/ทดลองใช้งาน-openshift-แบบฉบับ-local-cluster-พร้อมตัวอย่าง-app-ลอง-deploy-กัน-step-by-step-eff74bf41043
        - https://easynut.github.io/minishift101/README-th.html
        - https://medium.com/@nutta/วิธีการติดตั้ง-minishift-บน-windows-10-d4bee756c49d
        - https://medium.com/@nutta/ขั้นตอนการติดตั้ง-minishift-บน-macbook-macos-f1e138a32cf4

- firefox with proxy setup in remote pc
- download `oc cli`
    - window 
        > https://github.com/openshift/origin/releases/tag/v3.11.0
    - mac
        > https://github.com/openshift/origin/releases/tag/v3.11.0 
        ```sh
        ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
        brew install openshift-cli
        ```
