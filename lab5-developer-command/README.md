# course-openshift : developer-command

- คำสั่ง oc cli ในเชิง developer
    - login as developer
        ```
        oc login -u developer
        oc whoami
        oc get pod,svc,is,pvc,bc
        oc describe pod/pod_name
        oc rsh pod/pod_name
        oc logs pod/pod_name
        oc delete pod/pod_name
        ```
    - more command 
        ```
        oc -h
        ```
        - https://design.jboss.org/redhatdeveloper/marketing/openshift_cheatsheet/cheatsheet/images/openshift_cheat_sheet_r1v1.pdf
        - https://livebook.manning.com/book/openshift-in-action/oc-cheat-sheet/
        - https://cheatsheet.dennyzhang.com/cheatsheet-openshift-a4