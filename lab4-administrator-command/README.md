# course-openshift : administrator-command
- คำสั่งของ minishift ที่เอาไว้ควบคุมการทำงาน
    - start
    - status
    - stop
    - ip
    - ssh
    - exec


- คำสั่ง oc cli ในเชิง admin
    - login as system admin
        ```sh
        oc login -u system:admin
        ```
    - ตัวอย่างคำสั่ง
        ```sh 
        oc get node
        oc describe node/node_name
        oc adm config view
        oc adm -h
        ```
    - https://docs.openshift.com/container-platform/4.2/cli_reference/openshift_cli/administrator-cli-commands.html



